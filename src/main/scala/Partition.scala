import org.apache.spark.graphx.{Graph, VertexId}
import org.apache.spark.graphx.util.GraphGenerators
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import scala.collection.mutable.ListBuffer


object Partition {
  case class Vertex(nodeId: Long, adjacentNodes: List[Long])
  def main ( args: Array[String] ) {
    val conf = new SparkConf().setAppName("GraphXPartitioning");
    val sc = new SparkContext(conf);
    val nodes = sc.textFile(args(0)).map( line => {
      val s = line.split(",").toList
      new Vertex(s(0).toLong, s.tail.map(_.toLong))
    } )
    println("vertex", nodes)
    nodes.foreach(println)

    val edges: RDD[Edge[Long]] = nodes.flatMap( node => node.adjacentNodes.map(
                                  adj => ( node.nodeId, adj ))).map(a => {
                                        Edge(a._1,a._2, 0L)
                                      })

//    println("edges",edges);
//    edges.foreach(println)
//    var edgesRDD = sc.parallelize(edges)
    var count = 0;
    val listNodes = sc.textFile(args(0)).map(line => { val s = line.split(",").toList;
                                        var vertexLong = -1L;
                                        count += 1;
                                        if (count <= 5)
                                          vertexLong = s(0).toLong;
                                        vertexLong;
                                        } );
    val firstFiveNodes = listNodes.filter( _ != -1L).collect().toList;

    println("firstFiveNods: ", firstFiveNodes);
    val graphFromEdges : Graph[Long, Long] = Graph.fromEdges( edges , 0L)
      .mapVertices((id, _) => {
        var clusterId = -1L;
        if(firstFiveNodes.contains(id))
          clusterId = id
        clusterId
      })
    println("combined graph",graphFromEdges)
//    graphFromEdges.edges.foreach(println())
//    graphFromEdges.vertices.collect().foreach(println)

//    println("vertices", vertices.collect())
  // Depth = 6
  val valuesAfterNewClusterId = graphFromEdges.pregel(Long.MinValue, 6)(
                          ( vvertexId, vertexData, cluster) => {
                              if (vertexData == -1)
                                math.max(vertexData, cluster)
                              else
                                vertexData
                              },
                              triplet => { Iterator(( triplet.dstId, triplet.srcAttr )) },
                              (a, b) => math.max(a, b)
                          )

  var finalPartions = valuesAfterNewClusterId.vertices.map {
                                                        case (vertexId, centroid) => (centroid, 1)
                                                      }.reduceByKey( _ + _ )

    finalPartions.collect.foreach(println)

    sc.stop()

  }
}
